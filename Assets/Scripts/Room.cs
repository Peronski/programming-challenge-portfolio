using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SO.Room;
using NodeSystem;
using System.Linq;
using TMPro;
using Manager.Grid;

namespace Cell.Room
{
    public enum CardinalDirection
    {
        Nord = 0,
        Est = 1,
        Ovest = -1,
        Sud = -2
    }

    public class Room : MonoBehaviour
    {
        #region Private Variables
        private RoomType _roomType;
        private int _xCoord;
        private int _yCoord;
        private bool _isTarget;
        #endregion

        #region Public Variables
        [SerializeField] private TextMeshProUGUI m_nameRoom;
        [SerializeField] private SpriteRenderer m_spriteRenderer;
        [SerializeField] private GameObject m_doorPrefab;
        private Dictionary<CardinalDirection, GameObject> _dictionaryDoor = new Dictionary<CardinalDirection, GameObject>();
        #endregion

        #region Properties
        public RoomType RoomType => _roomType;
        public static int TotalDirections => CardinalDirection.GetNames(typeof(CardinalDirection)).Length;
        public int XCoord => _xCoord;
        public int YCoord => _yCoord;
        public bool IsTarget => _isTarget;
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        /// <summary>
        /// Configures TextMeshPro.Text taking capital letters from <paramref name="fullname"/>
        /// </summary>
        /// <param name="fullname"></param>
        /// <returns></returns>
        private string GetUpperLetters(string fullname)
        {
            string result = "";

            foreach(char c in fullname.Where(x => char.IsUpper(x)))
            {
                result += c;
            }

            return result;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns if the room has been already configured
        /// </summary>
        /// <returns></returns>
        public bool IsDefaultRoom()
        {
            return _roomType == 0;
        }

        /// <summary>
        /// Assigns coordinates x <paramref name="column"/> and y <paramref name="row"/> from grid matrix
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        public void AssignCoordinates(int column, int row)
        {
            _xCoord = column;
            _yCoord = row;
        }

        /// <summary>
        /// Configures room's infromation from given scriptable object <paramref name="roomSO"/>
        /// </summary>
        /// <param name="roomSO"></param>
        public void Configure(RoomSO roomSO)
        {
            _roomType = roomSO.Type;
            m_spriteRenderer.color = roomSO.Color;
            m_nameRoom.text = GetUpperLetters(_roomType.ToString());
        }

        public void Configure(RoomSO roomSO, bool isTarget)
        {
            _roomType = roomSO.Type;
            m_spriteRenderer.color = roomSO.Color;
            m_nameRoom.text = GetUpperLetters(_roomType.ToString());
            _isTarget = isTarget;
        }

        /// <summary>
        /// Spawns doors, one for each side and associates them to cardinal direction
        /// </summary>
        public void SpawnDoors()
        {
            for(int i = -(TotalDirections/2); i < (TotalDirections/2); ++i)
            {
                if (i % 2 == 0)
                {
                    Vector2 spawnPos = new Vector2(transform.position.x, (transform.position.y + Mathf.Sign(i) * ((GridManager.Instance.HeightDistance + GridManager.Instance.DefaultDistance)/2)));
                    GameObject door = Instantiate(m_doorPrefab, spawnPos, Quaternion.AngleAxis(90f, Vector3.forward));
                    door.SetActive(false);
                    _dictionaryDoor.Add((CardinalDirection)i, door);
                }
                else
                {
                    Vector2 spawnPos = new Vector2((transform.position.x + Mathf.Sign(i) * ((GridManager.Instance.WindthDistance + GridManager.Instance.DefaultDistance)/2)), transform.position.y);
                    GameObject door =  Instantiate(m_doorPrefab, spawnPos, Quaternion.AngleAxis(0f, Vector3.forward));
                    door.SetActive(false);
                    _dictionaryDoor.Add((CardinalDirection)i, door);
                }
            }
        }

        public void SetDoorStatus(CardinalDirection dir, bool status)
        {
            _dictionaryDoor[dir].SetActive(status);
        }
        #endregion
    }
}
