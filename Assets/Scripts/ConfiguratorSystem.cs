using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using Manager.Grid;
using Manager.Graph;
using Algorithms.Solutions;

namespace ConfiguratorSystem
{
    public class ConfiguratorSystem : GeneralSingleton<ConfiguratorSystem>
    {
        #region Private Variables
        private GridManager _gridManager;
        private GraphManager _graphManager;
        private SolutionAlgorithms _algorithms;
        #endregion

        #region Public Variables
        [Header("Grid's parameters")]
        [SerializeField][Min(3)] private int m_column;
        [SerializeField][Min(3)] private int m_row;

        [Header("Solution's parameters")]
        [Tooltip("Select one algorithm for a solution")][SerializeField] private Algorithm m_solutionAlgorithm;

        //[SerializeField] private bool m_bossTarget = true;
        //[SerializeField] private bool m_lootTarget;
        //[SerializeField] private bool m_allLootTargets;
        #endregion

        #region Properties
        public int Column => m_column;
        public int Row => m_row;
        #endregion

        #region Behaviour Callbacks
        protected override void Awake()
        {
            base.Awake();

            _gridManager = GetComponent<GridManager>();
            _graphManager = GetComponent<GraphManager>();
            _algorithms = GetComponent<SolutionAlgorithms>();

            StartCoroutine(Co_ConfigureSystem());
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Starts configuring grid's parameters
        /// </summary>
        private void ConfigureGrid()
        {
            _gridManager.ConfigureRowAndColumn(m_column, m_row);
        }

        /// <summary>
        /// Starts configuring nodes and rooms
        /// </summary>
        private void ConfigureGraph()
        {
            _graphManager.ConfigureNodesAndRooms();
        }

        /// <summary>
        /// Starts configuring node-direction links
        /// </summary>
        private void Associate()
        {
            _graphManager.FillingNodeDirectionDictionary();
        }

        /// <summary>
        /// Starts configuring active doors
        /// </summary>
        private void WallDoors()
        {
            _graphManager.SetActiveWallDoors();
        }

        private void Solution(Algorithm solution)
        {
            switch (solution)
            {
                case Algorithm.Breadth_First_Search: _algorithms.StartCo_BreadthFirstSearch();
                    break;
                case Algorithm.Depth_First_Search: _algorithms.StartCo_BreadthFirstSearch();
                    break;
                case Algorithm.Dijkstra: _algorithms.StartCo_Dijkstra();
                    break;
            }
        }

        /// <summary>
        /// Coroutine to configure all the system
        /// </summary>
        /// <returns></returns>
        private IEnumerator Co_ConfigureSystem()
        {
            ConfigureGrid();

            yield return null;

            ConfigureGraph();

            yield return null;

            Associate();

            yield return null;

            WallDoors();

            yield return null;

            Solution(m_solutionAlgorithm);

        }
        #endregion

        #region Public Methods
        #endregion
    }
}
