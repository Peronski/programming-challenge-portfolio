using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeSystem;
using Manager.Graph;
using System.Linq;
using Utility;

namespace Algorithms.Solutions
{
    public enum Algorithm
    {
        Breadth_First_Search = 0,
        Depth_First_Search = 1,
        Dijkstra = 2,
    }

    public class SolutionAlgorithms : MonoBehaviour
    {
        #region Private Variables
        #endregion

        #region Public Variables
        [SerializeField] private float m_step;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        public void StartCo_BreadthFirstSearch()
        {
            StartCoroutine(Co_BreadthFirstSearch());
        }

        public void StartCo_DepthFirstSearch()
        {
            StartCoroutine(Co_DepthFirstSearch());
        }

        public void StartCo_Dijkstra()
        {
            StartCoroutine(Co_Dijkstra());
        }

        private IEnumerator Co_BreadthFirstSearch()
        {
            Node ns = GraphManager.Instance.StartingNode;
            Node ne = GraphManager.Instance.EndNode;
            Node current = null;

            List<Node> visistedNodes = new List<Node>();
            Queue<Node> queue = new Queue<Node>();

            queue.Enqueue(ns);

            var waitTime = new WaitForSeconds(m_step);

            while (queue.Count > 0)
            {
                current = queue.Dequeue();

                if (current == ne)
                    break;

                foreach (Node neight in current.GetNeighbourNodes())
                {
                    if (visistedNodes.Contains(neight))
                        continue;

                    queue.Enqueue(neight);
                    visistedNodes.Add(neight);
                    Debug.DrawLine(current.transform.position, neight.transform.position, Color.black, 10000f);
                    yield return waitTime;
                }
            }
        }

        private IEnumerator Co_DepthFirstSearch()
        {
            Node ns = GraphManager.Instance.StartingNode;
            Node ne = GraphManager.Instance.EndNode;
            Node current = null;

            List<Node> visistedNodes = new List<Node>();
            Stack<Node> stack = new Stack<Node>();

            stack.Push(ns);

            var waitTime = new WaitForSeconds(m_step);

            while (stack.Count > 0)
            {
                current = stack.Pop();

                if (current == ne)
                    break;

                foreach (Node neight in current.GetNeighbourNodes())
                {
                    if (visistedNodes.Contains(neight))
                        continue;

                    stack.Push(neight);
                    visistedNodes.Add(neight);
                    Debug.DrawLine(current.transform.position, neight.transform.position, Color.black, 1000f);
                    yield return waitTime;
                }
            }
        }

        private IEnumerator Co_Dijkstra()
        {
            Node ns = GraphManager.Instance.StartingNode;
            Node ne = GraphManager.Instance.EndNode;
            Node current = null;

            List<Node> visistedNodes = new List<Node>();
            List<Node> openSet = new List<Node>();

            openSet.Add(ns);
            ns.Weight = 0f;

            var wait = new WaitForSeconds(m_step);

            while(openSet.Count > 0)
            {
                current = Utilities.FindNodeMinWeight(openSet);
                openSet.Remove(current);

                if (current == ne)
                {
                    openSet.Clear();
                    openSet.Add(current);
                    break;
                }

                foreach(var neight in current.GetNeighbourNodes())
                {
                    float distance = Vector3.SqrMagnitude(neight.transform.position - current.transform.position) + current.Weight;
                    float distanceNe = Vector3.SqrMagnitude(ne.transform.position - neight.transform.position);

                    if (distance < neight.Weight)
                        neight.Weight = distance;

                    if (openSet.Contains(neight))
                        continue;

                    openSet.Add(neight);
                }

                yield return null;

            }

            while(openSet.Count > 0)
            {
                current = openSet.Find(x => x.Weight == openSet.Select(x => x.Weight).Min());

                if (current == ns)
                    break;

                Node nearest = Utilities.FindNodeMinWeight(current.GetNeighbourNodes());

                openSet.Add(nearest);

                Debug.DrawLine(current.transform.position, nearest.transform.position, Color.black, 10000f);

                yield return wait;
            }

        }
        #endregion
    }
}
