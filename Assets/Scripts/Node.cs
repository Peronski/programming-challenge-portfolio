using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cell.Room;
using SO.Room;
using Data.Database;
using Utility;
using System.Linq;

namespace NodeSystem
{
    public class Node : MonoBehaviour
    {
        #region Private Variables
        private List<Node> _neighbourNodes = new List<Node>(); //list of neighbour node
        private Room _room; //node updates the room info
        private CircleCollider2D _collider;
        private Dictionary<CardinalDirection, Node> _dictionaryNode = new Dictionary<CardinalDirection, Node>();
        private bool _visited;
        private float _weight = Mathf.Infinity;
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        public bool IsDefault => _room.IsDefaultRoom();
        public RoomType Type => _room.RoomType;
        public bool IsTarget => _room.IsTarget;
        public Dictionary<CardinalDirection, Node> DictionaryNode => _dictionaryNode;
        public bool Visited { get => _visited; set => _visited = value; }
        public float Weight { get => _weight; set => _weight = value; }
        #endregion

        #region Behaviour Callbacks
        private void Awake()
        {
            _room = GetComponent<Room>();
            _collider = GetComponent<CircleCollider2D>();

            Utilities.DefaultDictionaryNode(_dictionaryNode);
        }

        private void Start()
        {
            _room.SpawnDoors();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var node = other.gameObject.GetComponent<Node>();

            if (node != null && !_neighbourNodes.Contains(node))
                AddNode(node);
        }
        #endregion

        #region Private Methods
        public override string ToString()
        {
            return $"Room coordinates: [{_room.XCoord},{ _room.YCoord}]"; ;
        }
        #endregion

        #region Public Methods

        #region Node - Room Utility
        /// <summary>
        /// Add <paramref name="node"/> to neighbour list
        /// </summary>
        /// <param name="node"></param>
        public void AddNode(Node node)
        {
            _neighbourNodes.Add(node);
        }

        /// <summary>
        /// Configures associated room if it's default using scriptable object <paramref name="roomSO"/>
        /// </summary>
        /// <param name="roomSO"></param>
        public void ConfigureRoom(RoomSO roomSO)
        {
            if (_room.IsDefaultRoom())
                _room.Configure(roomSO);
        }

        /// <summary>
        /// Configures associated room if it's default using scriptable object <paramref name="roomSO"/> and sets if <paramref name="isTarget"/> in pathfinding
        /// </summary>
        /// <param name="roomSO"></param>
        /// <param name="isTarget"></param>
        public void ConfigureRoom(RoomSO roomSO, bool isTarget)
        {
            if (_room.IsDefaultRoom())
                _room.Configure(roomSO, isTarget);
        }

        /// <summary>
        /// Set <paramref name="status"/> of gameObject's collider
        /// </summary>
        /// <param name="status"></param>
        public void SetColliderStatus(bool status)
        {
            _collider.enabled = status;
        }
        #endregion

        #region Utility Methods On Neighbours Nodes
        /// <summary>
        /// Returns the amount of neighbour nodes
        /// </summary>
        /// <returns></returns>
        public int GetAmountNeighbours()
        {
            return _neighbourNodes.Count;
        }

        /// <summary>
        /// Returns the list of neighbour nodes
        /// </summary>
        /// <returns></returns>
        public List<Node> GetNeighbourNodes()
        {
            return _neighbourNodes;
        }

        /// <summary>
        /// Returns a list of neighbour nodes with a default room associtated
        /// </summary>
        /// <returns></returns>
        public List<Node> GetDefaultNeighbour()
        {
            //return _neighbourNodes.FindAll(x => x.IsDefault).ToList();

            List<Node> defaults = new List<Node>();

            for (int i = 0; i < _neighbourNodes.Count; ++i)
            {
                if (_neighbourNodes[i].IsDefault)
                {
                    defaults.Add(_neighbourNodes[i]);
                }
            }

            return defaults;
        }

        /// <summary>
        /// Verify if all neighbour nodes are configured (not default)
        /// </summary>
        /// <returns></returns>
        public bool AllNeighboursAreConfigured()
        {
            for (int i = 0; i < _neighbourNodes.Count; ++i)
            {
                if (_neighbourNodes[i].IsDefault)
                {
                    return false;
                }
            }

            return true;
        }
        #endregion


        #region Cardinal Directions - Doors Utility
        /// <summary>
        /// Sets active doors basing on no-presence neightbour
        /// </summary>
        public void SetActiveWallDoors()
        {
            foreach(CardinalDirection c in _dictionaryNode.Keys)
            {
                if(_dictionaryNode[c] == null)
                {
                    _room.SetDoorStatus(c, true);
                }
            }
        }
        #endregion

        #region Configuration General Rules
        /// <summary>
        /// Configures all rooms of neighbour nodes if they are default, using specific relations with roomtype selected node 
        /// </summary>
        public void ConfigureNeighboursRoom()
        {
            foreach (Node n in GetDefaultNeighbour())
            {
                RoomType typeNeight = Database.Instance.GetRoomTypeByNeighbour(Type);
                n.ConfigureRoom(Database.Instance.GetRoomByType(typeNeight));
            }
        }

        /// <summary>
        /// Configures associated room if it is default, using specific relations with roomtype passed <paramref name="node"/>
        /// </summary>
        /// <param name="node"></param>
        public void ConfigureByNode(Node node)
        {
            RoomType type = Database.Instance.GetRoomTypeByNeighbour(node.Type);
            ConfigureRoom(Database.Instance.GetRoomByType(type));
        }
        #endregion

        #endregion
    }
}
