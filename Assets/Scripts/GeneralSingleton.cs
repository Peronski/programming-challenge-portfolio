using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    public class GeneralSingleton<T> : MonoBehaviour where T : GeneralSingleton<T>
    {
        #region Private Variables
        private static T _instance;
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        public static T Instance => _instance;
        #endregion

        #region Behaviour Callbacks
        protected virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = (T)this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion
    }
}
