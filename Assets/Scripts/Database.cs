using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using Manager.Graph;
using SO.Room;

namespace Data.Database
{
    public class Database : GeneralSingleton<Database>
    {
        #region Private Variables
        private Dictionary<RoomType, RoomSO> _dictionaryRoomTypeSO = new Dictionary<RoomType, RoomSO>();
        private Dictionary<RoomType, RoomType> _dictionaryRoomTypeNeight = new Dictionary<RoomType, RoomType>();
        #endregion

        #region Public Variables
        [Header("Lists for Dictionary<RoomType, RoomSO> initialization")]
        [Tooltip("Order of scriptable should be the same of RoomType")]public List<RoomSO> m_roomSOList = new List<RoomSO>();
        [Tooltip("Order of enum should be the same of RoomSO")]public List<RoomType> m_roomTypeList = new List<RoomType>();
        public List<RoomType> m_roomTypeNeightList = new List<RoomType>();
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        protected override void Awake()
        {
            base.Awake();

            Utilities.SerializeDictionary(_dictionaryRoomTypeSO, m_roomTypeList, m_roomSOList);
            Utilities.SerializeDictionary(_dictionaryRoomTypeNeight, m_roomTypeList, m_roomTypeNeightList);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns Room scriptable object related to passed <paramref name="type"/>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public RoomSO GetRoomByType(RoomType type)
        {
            return _dictionaryRoomTypeSO[type];
        }

        /// <summary>
        /// Returns neighbour type relative to <paramref name="type"/> given
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public RoomType GetRoomTypeByNeighbour(RoomType type)
        {
            return _dictionaryRoomTypeNeight[type];
        }

        /// <summary>
        /// Returns randomly a Room scriptable object
        /// </summary>
        /// <returns></returns>
        public RoomSO GetRandomRoom()
        {
            return m_roomSOList[Random.Range(0, m_roomSOList.Count)];
        }

        /// <summary>
        /// Returns randomly a Room scriptable object related to <paramref name="typeOne"/> or <paramref name="typeTwo"/>
        /// </summary>
        /// <param name="typeOne"></param>
        /// <param name="typeTwo"></param>
        /// <returns></returns>
        public RoomSO GetRandomRoomBetweenSelectedType(RoomType typeOne, RoomType typeTwo)
        {
            List<RoomSO> forSelection = new List<RoomSO>();

            RoomSO one = GetRoomByType(typeOne);
            RoomSO two = GetRoomByType(typeTwo);

            forSelection.Add(two);
            forSelection.Add(one);

            return forSelection[Random.Range(0, 2)];
        }
        #endregion
    }
}
