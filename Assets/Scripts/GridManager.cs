using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using Cell.Room;
using NodeSystem;
using Manager.Graph;

namespace Manager.Grid
{
    public class GridManager : SceneSingleton<GridManager>
    {
        #region Private Variables
        //Grid's structure
        private int _row;
        private int _column;

        //Distance between cells
        private float _heightDistance; //distance based on cell's scale y
        private float _windthDistance; //distance based on cell's scale x

        //Room container
        private Room[,] _roomMatrix;
        #endregion

        #region Public Variables
        [Header("Cell prefab")]
        [SerializeField] private Room m_room; //default room
 
        [Header("Parameters")]
        [SerializeField] private float m_defaultDistance;
        [SerializeField] private Transform m_startingPosition;
        #endregion

        #region Properties
        public float HeightDistance => _heightDistance; 
        public float WindthDistance => _windthDistance;
        public float DefaultDistance => m_defaultDistance;
        public int Column => _column;
        public int Row => _row;
        #endregion

        #region Delegates
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        /// <summary>
        /// Default function to create a grid
        /// </summary>
        private void CreateGrid()
        {
            _heightDistance = m_room.transform.localScale.y;
            _windthDistance = m_room.transform.localScale.x;

            for (int i = 0; i < _row; ++i)
            {
                for (int j = 0; j < _column; ++j)
                {
                    Vector3 translation = new Vector3(j * (_windthDistance + m_defaultDistance), -(i * (_heightDistance + m_defaultDistance)), 0);

                    var room = Instantiate(m_room, m_startingPosition.position + translation, Quaternion.identity);
                    _roomMatrix[j, i] = room;
                    room.AssignCoordinates(j, i);

                    GraphManager.Instance.AddNode(room.GetComponent<Node>());
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Configure grid's <paramref name="column"/> and <paramref name="row"/>
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        public void ConfigureRowAndColumn(int column, int row)
        {
            _row = column;
            _column = row;
            _roomMatrix = new Room[_column, _row];

            CreateGrid();
        }
        #endregion
    }
}
