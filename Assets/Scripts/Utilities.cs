using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cell.Room;
using NodeSystem;
using System.Linq;

namespace Utility
{
    public class Utilities : MonoBehaviour
    {
        /// <summary>
        /// Calcolates dot product between given <paramref name="vector"/> and Vector3.Up
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static float VectorDownDot(Vector3 vector)
        {
            return Vector3.Dot(vector, Vector3.down);
        }

        /// <summary>
        /// Calcolates dot product between given <paramref name="vector"/> and Vector3.Right
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static float VectorLeftDot(Vector3 vector)
        {
            return Vector3.Dot(vector, Vector3.left);
        }

        /// <summary>
        /// Returns the node with minimum weight inside <paramref name="list"/>
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static Node FindNodeMinWeight(List<Node> list)
        {
            return list.Find(x => x.Weight == list.Select(x => x.Weight).Min());
        }

        /// <summary>
        /// Returns the node with maximum weight inside <paramref name="list"/>
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static Node FindNodeMaxWeight(List<Node> list)
        {
            return list.Find(x => x.Weight == list.Select(x => x.Weight).Max());
        }

        /// <summary>
        /// Serializes <paramref name="dictionary"/> keys using <paramref name="listKeys"/> and values using <paramref name="listValues"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="listKeys"></param>
        /// <param name="listValues"></param>
        public static void SerializeDictionary<T, V>(Dictionary<T, V> dictionary, List<T> listKeys, List<V> listValues)
        {
            if (listKeys.Count != listValues.Count)
                return;

            for (int i = 0; i < listKeys.Count; ++i)
            {
                dictionary.Add(listKeys[i], listValues[i]);
            }
        }

        /// <summary>
        /// Generic function where <typeparamref name="T"/> : Monobehaviour. Using dot product between two object, this method associates <paramref name="secondObj"/> to relative cardinal direction in <paramref name="dictionary"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="obj"></param>
        /// TODO: REVIEW
        public static void AssociateObjectCardinalDirection<T>(Dictionary<CardinalDirection, T> dictionary, T firstObj, T secondObj) where T : MonoBehaviour
        {
            Vector2 distance = firstObj.transform.position - secondObj.transform.position;

            float dotV = VectorDownDot(distance);

            if (dotV != 0)
            {
                if (dotV >= 1f)
                {
                    dictionary[CardinalDirection.Nord] = secondObj;
                    Debug.Log($"Stanza1 {firstObj} assegna a NORD la stanza2 {secondObj}");
                }
                else
                {
                    dictionary[CardinalDirection.Sud] = secondObj;
                    Debug.Log($"Stanza1 {firstObj} assegna a SUD la stanza2 {secondObj}");
                }
            }
            else
            {
                float dotH = VectorLeftDot(distance);

                if (dotH >= 1f)
                {
                    dictionary[CardinalDirection.Est] = secondObj;
                    Debug.Log($"Stanza1 {firstObj} assegna a EST la stanza2 {secondObj}");
                }
                else
                {
                    dictionary[CardinalDirection.Ovest] = secondObj;
                    Debug.Log($"Stanza1 {firstObj} assegna a OVEST la stanza2 {secondObj}");
                }
            }
        }

        /// <summary>
        /// Generic function where <typeparamref name="T"/> : Monobehaviour and <typeparamref name="U"/> : Object. Using dot product between two object, this method associates <paramref name="secondObj"/> to relative cardinal direction in <paramref name="dictionary"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="obj"></param>
        public static void AssociateObjectCardinalDirection<T, U>(Dictionary<CardinalDirection, U> dictionary, T firstObj, U secondObj) where T : MonoBehaviour where U : Object
        {
            var sObj = secondObj as GameObject;
            Vector2 distance = firstObj.transform.position - sObj.transform.position;

            float dotV = VectorDownDot(distance);

            if (dotV <= 0f)
            {
                if (dotV >= 1f)
                    dictionary.Add(CardinalDirection.Nord, secondObj);
                else
                    dictionary.Add(CardinalDirection.Sud, secondObj);
            }
            else
            {
                float dotH = VectorLeftDot(distance);

                if (dotH >= 1f)
                    dictionary.Add(CardinalDirection.Est, secondObj);
                else
                    dictionary.Add(CardinalDirection.Ovest, secondObj);
            }
        }

        /// <summary>
        /// Set <paramref name="dictionary"/>'s node as null foreach cardinal direction
        /// </summary>
        /// <param name="dictionary"></param>
        public static void DefaultDictionaryNode(Dictionary<CardinalDirection, Node> dictionary)
        {
            dictionary.Add(CardinalDirection.Nord, null);
            dictionary.Add(CardinalDirection.Sud, null);
            dictionary.Add(CardinalDirection.Est, null);
            dictionary.Add(CardinalDirection.Ovest, null);
        }


        /// <summary>
        /// Returns the opposite direction respect the given <paramref name="dir"/>
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public static CardinalDirection ReturnOppositeDirection(CardinalDirection dir)
        {
            if (dir == CardinalDirection.Nord)
                return CardinalDirection.Sud;
            else if (dir == CardinalDirection.Sud)
                return CardinalDirection.Nord;
            else if (dir == CardinalDirection.Est)
                return CardinalDirection.Ovest;
            else if (dir == CardinalDirection.Ovest)
                return CardinalDirection.Est;

            return (CardinalDirection)2; //TOHACK;: it could return ERROR! REFACTORY
        }

        /// <summary>
        /// Finds midpoint between <paramref name="firstVec"/> and <paramref name="secondVec"/>
        /// </summary>
        /// <param name="firstVec"></param>
        /// <param name="secondVec"></param>
        /// <returns></returns>
        public static Vector2 Midpoint(Vector2 firstVec, Vector2 secondVec)
        {
            var midpointX = (secondVec.x + firstVec.x) / 2;
            var midpointY = (secondVec.y + firstVec.y) / 2;
            return new Vector2(midpointX, midpointY);
        }
        
    }
}
