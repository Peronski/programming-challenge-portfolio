using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeSystem;
using Data.Database;
using SO.Room;
using Utility;
using Manager.Grid;

namespace Manager.Graph
{
    public class GraphManager : SceneSingleton<GraphManager>
    {
        #region Private Variables
        private List<Node> _graphList = new List<Node>();
        private Node _startingNode;
        private Node _endNode;
        private List<Node> _loots = new List<Node>();
        #endregion

        #region Public Variables
        #endregion

        #region Properties
        public Node StartingNode => _startingNode;
        public Node EndNode => _endNode; 
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        /// <summary>
        /// Starts the specialization of all rooms, recognizing all corners and defining randomly one of them as StartNode
        /// </summary>
        private void GetRandomStartingPoint()
        {
            List<Node> corners = new List<Node>();

            foreach(Node n in _graphList)
            {
                if(n.GetAmountNeighbours() <= 2)
                {
                    corners.Add(n);
                }
            }

            _startingNode = corners[Random.Range(0, corners.Count)];
            corners.Remove(_startingNode);

            RoomSO entry = Database.Instance.GetRoomByType(RoomType.Entry);

            _startingNode.ConfigureRoom(entry); 
            _startingNode.ConfigureNeighboursRoom(); 

            GetEndPoint(_startingNode, corners);
            SetTypeRemainingCorners(corners);
        }

        /// <summary>
        /// Using a <paramref name="startingPoint"/> determinates the farthest node as EndNode, between the list <paramref name="corners"/>
        /// </summary>
        /// <param name="startingPoint"></param>
        /// <param name="corners"></param>
        private void GetEndPoint(Node startingPoint, List<Node> corners)
        {
            Vector3 distance = Vector3.zero;

            foreach(Node n in corners)
            {
                Vector3 distanceSE = startingPoint.transform.position - n.transform.position;

                if (distanceSE.sqrMagnitude > distance.sqrMagnitude)
                {
                    distance = distanceSE;
                    _endNode = n;
                }
            }

            //TOHACK
            //configuration camera position
            Vector3 screenCenter = Utilities.Midpoint(_startingNode.transform.position, _endNode.transform.position);
            Camera.main.transform.position = screenCenter;
            Camera.main.orthographicSize = 0.55f * GridManager.Instance.Row;

            corners.Remove(_endNode);

            RoomSO boss = Database.Instance.GetRoomByType(RoomType.Boss);

            _endNode.ConfigureRoom(boss);
            _endNode.ConfigureNeighboursRoom();
        }

        /// <summary>
        /// Using a list of two <paramref name="corners"/> (no start/end corner) configures the loot, the border and diagonal rooms type
        /// </summary>
        /// <param name="corners"></param>
        private void SetTypeRemainingCorners(List<Node> corners)
        {
            RoomSO loot = Database.Instance.GetRoomByType(RoomType.Loot);

            foreach(Node n in corners)
            {
                n.ConfigureRoom(loot);
                _loots.Add(n);
            }

            SetDiagonalNodes(corners);
            SetAllRemainingDefaultRoom();
        }

        /// <summary>
        /// Using a list of two <paramref name="corners"/> finds all nodes between with a raycast and configures them choosing randomly RoomType easy or hard
        /// </summary>
        /// <param name="corners"></param>
        private void SetDiagonalNodes(List<Node> corners)
        {
            for (int i = 0; i < corners.Count; ++i)
                corners[i].SetColliderStatus(false);

            Vector2 start = new Vector2(corners[0].transform.position.x, corners[0].transform.position.y);
            Vector2 end = new Vector2(corners[1].transform.position.x, corners[1].transform.position.y);

            RaycastHit2D[] hit = Physics2D.LinecastAll(start, end);

            ConfigureHitNodes(hit, RoomType.Easy, RoomType.Hard);

            for (int i = 0; i < corners.Count; ++i)
                corners[i].SetColliderStatus(true);
        }

        /// <summary>
        /// Starts the waterfall fill of all default rooms remained
        /// </summary>
        private void SetAllRemainingDefaultRoom()
        {
            WaterfallFill(_startingNode.GetNeighbourNodes()[0]);
            WaterfallFill(_endNode.GetNeighbourNodes()[0]);
        }

        /// <summary>
        /// Using a list of <paramref name="corners"/> defines borders of grid and each type of each room whose are composed
        /// </summary>
        /// <param name="corners"></param>
        private void SetBorderNodes(List<Node> corners) //NOT USED
        {
            for (int i = 0; i < corners.Count; ++i)
                corners[i].SetColliderStatus(false);

            RaycastHit2D[] hitF = Physics2D.LinecastAll(corners[0].transform.position, _startingNode.transform.position);
            ConfigureHitNodes(hitF, RoomType.Easy);

            RaycastHit2D[] hitS = Physics2D.LinecastAll(corners[1].transform.position, _startingNode.transform.position);
            ConfigureHitNodes(hitS, RoomType.Easy);

            RaycastHit2D[] hitT = Physics2D.LinecastAll(corners[0].transform.position, _endNode.transform.position);
            ConfigureHitNodes(hitT, RoomType.Hard);

            RaycastHit2D[] hitFh = Physics2D.LinecastAll(corners[1].transform.position, _endNode.transform.position);
            ConfigureHitNodes(hitFh, RoomType.Hard);

            for (int i = 0; i < corners.Count; ++i)
                corners[i].SetColliderStatus(true);
        }

        /// <summary>
        /// Fills Dictionary<CardinalDirection, Node> of each node in graph
        /// </summary>
        public void FillingNodeDirectionDictionary()
        {
            foreach(Node n in _graphList)
            {
                foreach(Node m in n.GetNeighbourNodes())
                {
                    Utilities.AssociateObjectCardinalDirection<Node>(n.DictionaryNode, n, m);
                }
            }
        }

        /// <summary>
        /// Sets active doors for each node in graph
        /// </summary>
        public void SetActiveWallDoors()
        {
            foreach(Node n in _graphList)
            {
                n.SetActiveWallDoors();
            }
        }

        /// <summary>
        /// Objects of list <paramref name="hitNodes"/> are find with raycast and their rooms configured with <paramref name="typeOne"/>
        /// </summary>
        /// <param name="hitNodes"></param>
        /// <param name="typeOne"></param>
        private void ConfigureHitNodes(RaycastHit2D[] hitNodes, RoomType typeOne)
        {
            for (int i = 0; i < hitNodes.Length; ++i)
            {
                Node hitted = hitNodes[i].collider.gameObject.GetComponent<Node>();

                if (hitted != null)
                {
                    hitted.ConfigureRoom(Database.Instance.GetRoomByType(typeOne));
                }
            }
        }

        /// <summary>
        /// /// Objects of list <paramref name="hitNodes"/> are find with raycast and their rooms configured with <paramref name="typeOne"/> or <paramref name="typeTwo"/> randomly
        /// </summary>
        /// <param name="hitNodes"></param>
        /// <param name="typeOne"></param>
        /// <param name="typeTwo"></param>
        private void ConfigureHitNodes(RaycastHit2D[] hitNodes, RoomType typeOne, RoomType typeTwo)
        {
            for (int i = 0; i < hitNodes.Length; ++i)
            {
                Node hitted = hitNodes[i].collider.gameObject.GetComponent<Node>();

                if (hitted != null)
                {
                    hitted.ConfigureRoom(Database.Instance.GetRandomRoomBetweenSelectedType(typeOne, typeTwo));
                }
            }
        }

        /// <summary>
        /// Recursive method which verifies the presence of default neighbours of given <paramref name="node"/>, configures them on node parameter and goes depth into neighbours too, until all nodes are configured;
        /// </summary>
        /// <param name="node"></param>
        private void WaterfallFill(Node node)
        {
            if (node.IsDefault)
                return;

            if (node.AllNeighboursAreConfigured())
                return;

            foreach(Node n in node.GetDefaultNeighbour())
            {
                n.ConfigureByNode(node);
                WaterfallFill(n);
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds <paramref name="node"/> to graph's track node list
        /// </summary>
        /// <param name="node"></param>
        public void AddNode(Node node)
        {
            _graphList.Add(node);
        }

        /// <summary>
        /// Calls the private function GetRandomStartingPoint, starting the configuration of rooms and nodes 
        /// </summary>
        public void ConfigureNodesAndRooms()
        {
            GetRandomStartingPoint();
        }
        #endregion
    }
}
