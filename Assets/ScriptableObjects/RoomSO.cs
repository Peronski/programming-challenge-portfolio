using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum RoomType
{
    Entry = 1,
    PostEntry,
    Boss,
    PreBoss,
    Loot,
    Easy,
    Hard
}

namespace SO.Room
{
    [CreateAssetMenu(fileName = "New SO Room", menuName = "Asset/ScriptableObjects/Rooms")]
    public class RoomSO : ScriptableObject
    {
        #region Private Variables
        #endregion

        #region Public Variables
        [Header("Attributes")]
        [SerializeField] private RoomType m_type;
        [SerializeField] private Color m_color;
        #endregion

        #region Properties
        public RoomType Type => m_type;
        public Color Color => m_color;
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
